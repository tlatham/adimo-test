public class Product {
    private String title;
    private String imgurl;
    private double price;
    private double discount;

    public Product(String title, String imgurl, double price, double discount){
        this.title=title;
        this.imgurl=imgurl;
        this.price=price;
        this.discount=discount;
    }
    public Product(String title, String imgurl, double price){
        this.title=title;
        this.imgurl=imgurl;
        this.price=price;
        this.discount=0.0;
    }

    public String getTitle() {
        return title;
    }

    public String getImgurl() {
        return imgurl;
    }

    public double getPrice() {
        return price;
    }

    public double getDiscount() {
        return discount;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setDiscount(double discount) {
        this.discount=discount;
    }
}
