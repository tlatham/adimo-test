import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Scraper {
        ArrayList<Product> products = new ArrayList<>();
        double priceAvg;
        double totalPrice;

        public void Scrape(String url){
            try{
                Document d = Jsoup.connect(url).get();
                Elements e = d.select("div#details");
                for (Element ele : e.select("div.item")){
                    String imgurl = ele.select("img").attr("src");
                    String title = ele.select("h1").text();
                    Double price = Double.parseDouble(ele.select("span.price").text().substring(1));
                    totalPrice+=price;
                    if (!ele.select("span.oldPrice").isEmpty()){
                        double oldPrice = Double.parseDouble(ele.select("span.oldPrice").text().substring(1));
                        products.add(new Product(title, imgurl, price, (oldPrice-price)));
                    } else{
                        products.add(new Product(title, imgurl, price));
                    }
                }
                priceAvg=totalPrice/products.size();
            } catch (IOException e){
                System.out.println(e);
            }
        }

        public void extractToJson(){
            JSONArray details = new JSONArray();
            for (Product p: products){
                JSONObject product = new JSONObject();
                product.put("title", p.getTitle());
                product.put("imageURL", p.getImgurl());
                product.put("price", p.getPrice());
                if (p.getDiscount()>0.0){
                    product.put("discount", p.getDiscount());
                }
                details.add(product);
            }
            JSONObject total = new JSONObject();
            total.put("total", products.size());
            details.add(total);

            JSONObject avg = new JSONObject();
            avg.put("averagePrice", priceAvg);
            details.add(avg);

            try{
                FileWriter f = new FileWriter("details.json");
                f.write(details.toJSONString());
                f.flush();
            } catch (IOException e){
                System.out.println(e);
            }


        }

        public static void main(String[] args){
            Scraper s = new Scraper();
            s.Scrape("https://cdn.adimo.co/clients/Adimo/test/index.html");
            s.extractToJson();
        }
        //TODO tests for the functions and product class
        //TODO write the json output better to group the products as an array
        //TODO Modularise the code properly, returning the elements object and manipulate elsewhere,
        // as well as specifying the json output file as parameter
        //TODO format currency properly


}
